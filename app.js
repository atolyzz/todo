const inputTaskRef = document.querySelector("#inputTask");
const buttonRef = document.querySelector("#btnAddTask");
const taskListRef = document.querySelector("#taskList");
const url = "http://localhost:3000/todos/";

buttonRef.addEventListener("click", function(event) {
  event.preventDefault();
  const task = {
    title: inputTaskRef.value,
    status: "active"
  };

  inputTaskRef.value = "";

  saveToDb(task);
  // addDataToLocalStorage(task);
  console.log(inputTaskRef.value);
});

function createTask(task) {
  const li = document.createElement("li");
  const liTekst = document.createTextNode(task.title);

  li.classList.add(task.status);
  li.classList.add("task-item");

  const buttonRemove = document.createElement("button");
  const buttonRemoveText = document.createTextNode("remove");
  buttonRemove.appendChild(buttonRemoveText);
  buttonRemove.addEventListener("click", function() {
    removeTask(this);
    deleteDateFromDb(task);
    // removeDataInLocalStorage(task.title);
  });

  const buttonDone = document.createElement("button");
  const buttonDoneText = document.createTextNode(task.status);
  buttonDone.appendChild(buttonDoneText);
  buttonDone.addEventListener("click", function() {
    changeStatus(this);
    updateDataInDb(task);
    // updateDateToLocalStorage(task.title);
  });

  li.appendChild(liTekst);
  li.appendChild(buttonDone);
  li.appendChild(buttonRemove);

  return li;
}

function changeStatus(button) {
  button.parentNode.classList.toggle("done");
  if (button.parentNode.classList.contains("done")) {
    button.innerText = "done";
  } else {
    button.innerText = "active";
  }
}

function removeTask(button) {
  button.parentNode.remove();
}

function addDataToLocalStorage(task) {
  const data = JSON.parse(localStorage.getItem("todos"));
  if (data) {
    data.push(task);
    localStorage.setItem("todos", JSON.stringify(data));
  } else {
    localStorage.setItem("todos", JSON.stringify([task]));
  }
}
function removeDatainLocalStorage(title) {
  const tasksInStorage = JSON.parse(localStorage.getItem("todos"));
  const tasksFiltered = tasksInStorage.filter(task => task.title !== title);
  localStorage.setItem("todos", JSON.stringify(tasksFiltered));
}

function getDataFromLocalStorage() {
  const tasks = JSON.parse(localStorage.getItem("todos"));
  if (tasks) {
    tasks.forEach(task => {
      const taskHTML = createTask(task);
      taskListRef.appendChild(taskHTML);
    });
  }
}
// getDataFromLocalStorage();

function updateDateToLocalStorage(title) {
  const tasksInStorage = JSON.parse(localStorage.getItem("todos"));
  const task = tasksInStorage.filter(el => el.title === title)[0];
  task.status === "active" ? (task.status = "done") : (task.status = "active");
  localStorage.setItem("todos", JSON.stringify(tasksInStorage));
}

function deleteDateFromDb(task) {
  fetch(`${url}${task.id}`, { method: "DELETE" }).then(response =>
    console.log(response)
  );
}

function updateDataInDb(task) {
  const newStatus = task.status === "active" ? "done" : "active";
  fetch(`${url}${task.id}`, {
    method: "PATCH",
    body: JSON.stringify({ status: newStatus }),
    headers: { "Content-Type": "application/json" }
  })
    .then(r => r.json())
    .then(r => console.log(r));
}

function saveToDb(task) {
  fetch(url, {
    method: "POST",
    body: JSON.stringify(task),
    headers: { "Content-Type": "application/json" }
  }).then(response =>
    response.json().then(response => {
      const taskHTML = createTask(task);
      taskListRef.appendChild(taskHTML);
    })
  );
}

function getDataFromDb() {
  fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => response.json())
    .then(resp => {
      resp.forEach(task => {
        const taskHTML = createTask(task);
        taskListRef.appendChild(taskHTML);
      });
    });
}

getDataFromDb();
